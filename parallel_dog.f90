program dog
  use mpi
  implicit none
  integer ierror, size, rank, i
  character (len=128) message
  
  call MPI_INIT(ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)

  write(*,*) 'WOOF :', rank, ' of ', size

!  call MPI_BARRIER( MPI_COMM_WORLD, ierror)
  
  write(6,*) 'Dog pack is syncronized'
  call flush(6)

  
  if(rank == 0) then
    message = "Attack!"
    write(6,*), "Alpha dogs says: ", trim(message)
    call flush(6)
    do i = 1, size -1
       call MPI_SEND(message, 128, MPI_CHAR, i, 1, MPI_COMM_WORLD, ierror)
    end do
 end if
 
!  call MPI_BARRIER( MPI_COMM_WORLD, ierror)
    
  if(rank > 0 .AND. rank <5) then
    call MPI_RECV(message, 128, MPI_CHAR, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE, ierror)
    write(6,*), "Dog number", rank, " gets the command: ", trim(message)
    call flush(6)
  end if
  
  call MPI_FINALIZE(ierror)
 
end program dog
