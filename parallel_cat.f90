program cat
  use mpi
  implicit none
  integer ierror, size, rank

  call MPI_INIT(ierror)
  call MPI_COMM_SIZE(MPI_COMM_WORLD, size, ierror)
  call MPI_COMM_RANK(MPI_COMM_WORLD, rank, ierror)

  write(*,*) 'MEOW :', rank

  call MPI_FINALIZE(ierror)
  

end program cat
